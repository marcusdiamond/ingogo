import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';


import { reducer } from '../trip/reducers/trip';
import { TripModule } from '../trip/trip.module';
import { ActionReducerMap } from '@ngrx/store';


export const reducers: ActionReducerMap<any> = {
  trip: reducer
};
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    StoreModule.forRoot(reducers),
    TripModule



  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
