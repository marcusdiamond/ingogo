import { Action } from '@ngrx/store';
import { Trip } from '../models/trip';

export const ADD_TRIP = '[TRIP] add trip';

export const SELECT_TRIP = '[TRIP] select trip';



export class Add implements Action {
    readonly type = ADD_TRIP;
    constructor(public trip: Trip) { }
}

export class Select implements Action {
    readonly type = SELECT_TRIP;
    constructor(public id: string) { }
}

export type TripActions =
    | Add
    | Select;
