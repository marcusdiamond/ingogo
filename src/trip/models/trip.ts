import * as moment from 'moment';
export interface Trip {
    id: string;
    mobile: string;
    pickup: any;
    destination: any;
    pickUpFormattedAddress: string;
    destinationFormattedAddress: string;
    pickupLocalTime: moment.Moment;
    pickupOffset: number;
    duration: string;
}
