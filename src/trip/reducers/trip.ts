import { createSelector, createFeatureSelector } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Trip } from '../models/trip';
import * as trip from '../actions/trip';

export interface State extends EntityState<Trip> {
    selectedTripId: string | null;
}

// create new adapter
export const adapter: EntityAdapter<Trip> = createEntityAdapter<Trip>({
    selectId: (trip: Trip) => trip.id
});

// set the initial state of the app
export const initialState: State = adapter.getInitialState({
    selectedTripId: null,
    ids: []
})

// this function is called after every execution of a action
export function reducer(
    state = initialState,
    action: trip.TripActions
): State {

    switch (action.type) {

        case trip.ADD_TRIP: {
            return adapter.addOne(action.trip, state);
        }





        case trip.SELECT_TRIP: {
            return {
                ...state,
                selectedTripId: action.id
            };
        }

        default: {
            return state;
        }
    }

}

export const selectedId = (state: State) => state.selectedTripId;

// selectors
export const getTripsState = createFeatureSelector<State>('trips');

/**
 * Create new selector to watch changes on entities
 */
export const getTripsEntitiesState = createSelector(
    getTripsState,
    state => state.entities
);

/**
 * Create new selector to watch change on selectedTripId.
 * Feel lines above, you can see where we create the const selectedId
 */
export const getSelectedId = createSelector(
    getTripsState,
    selectedId
);

/**
 * This is the basics selectors that we can create using the adapter.
 * This is only possible if you are using @ngrx/entity. Without @ngrx/entity,
 * you have to create every selector you want.
 */
export const {
    selectIds: getTripsIds,
    selectEntities: getTripsEntities,
    selectAll: getTripsAll,
    selectTotal: getTripsTotal
} = adapter.getSelectors(getTripsState);

/**
 * Create new selector to whatch changes on selectedId
 * and return the entity of that id
 */
export const getSelectedTrip = createSelector(
    getTripsEntitiesState,
    getSelectedId,
    (entities, id) => {
        return entities[id] || null
    }
)