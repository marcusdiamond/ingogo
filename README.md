

Ben apologies for delay, its been a crazy few days for me since thursday. I have had numerous interviews and sick kids to deal while my wife is away since receiving the challenge have only been able to sit down with in the last day. I enjoyed it, much more interesting then other ones I have done recently.


#Trade Offs/Compromises

Improve AutoComplete:
For times sake I used a 3rd party Google Autocomplete component. For production i would of used Angular Material Autocomplete using MapsAPILoader available in agm/core or straight httpclient to access the api as the datasource. This would of allowed better control of the formcontrol validation and better style matching for the autocomplete popup list. Also there is a need to use viewchilds to set focus to the next input upon autocomplete selection. Currently you can ignore the autocomplete and just type into the input and tab out and the field is still valid, this would be fixed for production  as well with a custom form component.

DateTimePicker
This was a suitable 3rd party component but more refinement needed. Set focus on this input does not always bring up the pop up as well and it goes dirty and invalid as soon as you set focus which shows it as red before the user selects a datetime.

Update, Delete of Trips
As it was not scoped trip update and delete was not implement via ngrx/store and component level. Ideally users should be able to update or delete trips. Currently selecting trip sets the form as disabled to not allow editing.

UI Improvements
Trip list needs improving and overall responsive layout optimizatiton  for a responsive experience on all devices. Use of icons 

Further Componentisation
The trip form, trip list & items, map & duration/distance could all be made into individual components

Angular Universal
User Universal to render the ui straight the server for faster user experience and play back user input once angular has load in.


#rxjs-compat

Due to some of the 3rd party libraries use of older rxjs and the latest rxjs there may be the need to: npm install rxjs-compat 
This should be dealt with before production 







