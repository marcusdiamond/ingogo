import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import * as fromTrips from './../reducers/trip';
import * as trip from './../actions/trip';
import * as moment from 'moment';
import { Trip } from './../models/trip';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';


// just for date picker format
export const MY_CUSTOM_FORMATS = {
    parseInput: 'LL LT',
    fullPickerInput: 'LL LT',
    datePickerInput: 'LL',
    timePickerInput: 'LT',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
};

@Component({
    selector: 'trips',
    template: `
    <trips-display
    [trips]="trips | async"
    [selectedTrip]="selectedTrip | async"
    (onSave)="save($event)"
    (onSelectTrip)="selectTrip($event)"
    (onNew)="newTrip()"
    ></trips-display>
    `
})


export class TripsParentComponent implements OnInit {
    trips: any;
    selectedTrip;
    constructor(private store: Store<any>) {
        this.trips = this.store.select(fromTrips.getTripsAll);
        this.selectedTrip = this.store.select(fromTrips.getSelectedTrip);
    }

    ngOnInit() { }

    save(payload) {
        this.store.dispatch(new trip.Add(payload));
        this.store.dispatch(new trip.Select('1'));
    }

    selectTrip(payload) {
        this.store.dispatch(new trip.Select(payload));
    }

    newTrip() {
        this.store.dispatch(new trip.Select('1'));
    }
}



@Component({
    selector: 'trips-display',
    templateUrl: './trip.html',
    styleUrls: ['./trip.scss'],
    providers: [
        { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS },
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class TripsChildComponent implements OnInit {
    directions;
    duration;
    distance;
    latitude;
    longitude;
    zoom;
    form: FormGroup;

    @Output() onSelectTrip = new EventEmitter();
    @Output() onSave = new EventEmitter();
    @Output() onNew = new EventEmitter();

    _selectedTrip;
    @Input()
    set selectedTrip(val) {
        if (val === null) {
            val = {};
            this.form.enable();
        } else {
            this.form.patchValue(val);
            this.form.disable();

        }
        this._selectedTrip = Object.assign({}, val);
        this.updateDirections();
    }
    get selectedTrip() {
        return this._selectedTrip;
    }

    @Input() trips;

    @ViewChild('timePicker')
    public timePickerElementRef: ElementRef;

    @ViewChild('pickupInput')
    public pickupInputElementRef: ElementRef;
    @ViewChild('destinationInput')
    public destinationInputElementRef: ElementRef;


    autoCompleteOptions = {
        types: [],
        componentRestrictions: { country: 'AU' }
    };


    constructor(private formBuilder: FormBuilder) {
        this.form = this.formBuilder.group({
            pickUpFormattedAddress: new FormControl('', Validators.required),
            destinationFormattedAddress: new FormControl('', Validators.required),
            pickupLocalTime: new FormControl('', Validators.required),
            mobile: new FormControl('',
                [Validators.pattern('^(?:\\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$')
                    , Validators.required])
        });
    }

    ngOnInit() {
        this.setCurrentPosition();
        this.pickupInputElementRef.nativeElement.focus();
    }


    destinationAddressChange(place) {
        this._selectedTrip.destination = place;
        this.updateDirections();

    }

    pickUpAddressChange(place) {
        this._selectedTrip.pickup = place;
        this.updateDirections();
        this.destinationInputElementRef.nativeElement.focus();
    }

    updateDirections() {
        // if both pick and destination are not null we will set directions
        this.directions = null;
        if (this._selectedTrip.pickup != null && this._selectedTrip.destination != null) {
            this.directions = {
                origin: { lat: this._selectedTrip.pickup.geometry.location.lat(), lng: this._selectedTrip.pickup.geometry.location.lng() },
                destination:
                    {
                        lat: this._selectedTrip.destination.geometry.location.lat(),
                        lng: this._selectedTrip.destination.geometry.location.lng()
                    }
            };
        }
    }



    private setCurrentPosition() {
        // just sets map lat lng locally for the second it appears before agm-direction overrides
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 12;
            });
        }
    }

    directionChange(dir) {
        // fetches details of duration and distance from agm-direction
        if (dir != null) {
            if (dir.routes[0]) {
                if (dir.routes[0].legs[0]) {
                    this.duration = dir.routes[0].legs[0].duration;
                    this.distance = dir.routes[0].legs[0].distance;
                }
            }
        }
    }


    save() {
        // get date with utc offset from pickup location
        const date = moment(this.timePickerElementRef.nativeElement.value).utcOffset(this._selectedTrip.pickup.utc_offset, true);

        const payload: Trip = {
            pickup: this._selectedTrip.pickup,
            destination: this._selectedTrip.destination,
            pickUpFormattedAddress: this._selectedTrip.pickup.formatted_address,
            destinationFormattedAddress: this._selectedTrip.destination.formatted_address,
            pickupLocalTime: date,
            pickupOffset: this._selectedTrip.pickup.utc_offset,
            duration: this.duration,
            mobile: this.form.controls['mobile'].value,
            id: this.getGuid(),
        };
        this.onSave.emit(payload);
        this.directions = null;
        this._selectedTrip.pickup = null;
        this._selectedTrip.destination = null;
        this.form.reset();
        this.pickupInputElementRef.nativeElement.focus();
    }

    new() {
        this.form.reset();
        this.onNew.emit();
        this.form.enable();
        this.pickupInputElementRef.nativeElement.focus();
    }





    // below 2 functions are just hacky way of producing guid
    S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    getGuid() {
        // then to call it, plus stitch in '4' in the third group
        let guid = (this.S4() + this.S4() + "-" + this.S4() + "-4" + this.S4().substr(0, 3) + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4()).toLowerCase();
        return guid;
    }



}

