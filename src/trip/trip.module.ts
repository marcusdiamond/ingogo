import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { CustomMaterialModule } from './../shared/material.module';

import { TripsParentComponent, TripsChildComponent } from './components/trip.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AgmDirectionModule } from 'agm-direction';
import { ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import * as trips from './reducers/trip';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { AgmCoreModule } from '@agm/core';
import { TripPipe } from './pipes/trip.pipes';
@NgModule({
    imports: [
        CommonModule,
        GooglePlaceModule,
        StoreModule.forFeature('trips', trips.reducer),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBhWzUopXwWko1H1Yp3F922uZiFET-8alc'
        }),
        AgmDirectionModule, CustomMaterialModule,
        FlexLayoutModule, ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule, OwlMomentDateTimeModule,

    ],
    declarations: [
        TripsParentComponent,
        TripsChildComponent,
        TripPipe

    ],
    exports: [
        TripsParentComponent,
        TripsChildComponent
    ]
})
export class TripModule { }
