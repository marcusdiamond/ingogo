import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
    name: 'tripDisplay'
})

export class TripPipe implements PipeTransform {
    transform(value: any, filter: string): any {
        switch (filter) {
            case 'suburb':
                try {
                    return value.address_components.find(component => component.types.includes('locality')).long_name;
                }
                catch (error) {
                    return value.formatted_address;
                }
                break;
            case 'iso-time':
                return value.pickupLocalTime.toISOString();
            case 'local-time':
                return value.pickupLocalTime.format('llll');
            case 'drop-off-time':


                return value.pickupLocalTime.clone().add(value.duration.value, 's').format('llll');

        }
    }
}
